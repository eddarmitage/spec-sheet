Feature: Command line arguments
  Tests the various flags and arguments that can be passed in to the
  specsheet command

  Scenario: Command can be run
    When specsheet is run
    Then the exit code is 0

  Scenario: Output current version
    When specsheet --version is run
    Then version is output

from subprocess import call
from tempfile import TemporaryFile


def execute(command, exit_status=0):
    """
    Run command.
    :param command: Command plus any arguments
    :type command: list of str or unicode
    :param exit_status: Expected exit status
    :type exit_status: int
    :return: (exit code, standard output and error)
    :rtype: (int, str or unicode)
    :raises AssertionError: if actual exit status does not match
     exit_status
    """
    actual_exit_status, stdout = _execute_and_capture(command)
    assert exit_status == actual_exit_status, "Unexpected exit code " + str(actual_exit_status) + "\n" + "".join(stdout)
    print("Output: ".join(stdout))
    return actual_exit_status, stdout


def _execute_and_capture(command):
    """
    Run a command via the operating system and capture and return
    standard output and standard error.
    :param command: Command to run plus any arguments
    :type command: list of str or unicode
    :return: (exit code, standard output and error)
    :rtype: (int, str or unicode)
    :raises OSError: if there are problems running the command
    """
    with TemporaryFile(mode="w+", suffix="log") as stdout:
        result = call(command, stdout=stdout, stderr=stdout)
        stdout.seek(0)
        return result, stdout.readlines()

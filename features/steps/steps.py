import re

from behave import when, then

from features.steps.helpers import execute


@when("{command} is run")
def run(context, command):
    code, output = execute(command.split() + [str(context.source_dir.name)])
    context.output += output
    context.exit_code = code


@then("the exit code is {code}")
def exit_code(context, code):
    assert context.exit_code == int(code)


@then("version is output")
def version_is_output(context):
    assert any(re.fullmatch(r"[0-9]+\.[0-9]+\.[0-9]+.*\n?", line) for line in context.output)

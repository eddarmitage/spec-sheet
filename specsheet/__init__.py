"""
Spec Sheet
~~~~~~
Command to produce navigable documentation from Gherkin feature files.

:copyright: Copyright 2019 Edward Armitage.
:license: MIT, see LICENSE for details.
"""
from ._version import get_versions

__version__ = get_versions()["version"]
del get_versions
